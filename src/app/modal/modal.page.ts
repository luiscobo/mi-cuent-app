import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {
 
  // Data passed in by componentProps
  @Input() total: number;
  @Input() numeroComensales: number;
  @Input() totalComensal: number;

  constructor(public modalCtrl:ModalController) { }

  ngOnInit() {
  }
  
  dismissModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
