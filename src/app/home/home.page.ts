import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  numComensales: number;
  importe: number;
  propina: any;
  total: number;
  totalComensal: number;
  mostrarRango:boolean = false;

  constructor(public modalCtrl:ModalController) {}

  /* Funcion para controlar cuando se hace un cambio de selección en el Radio Button de propina*/
  cambioValorPropina(event) {

    // Si se selecciona "otro" se habilita el componente de seleccion libre de propina*/
    if(event.detail.value=="otro"){
      this.mostrarRango = true;

    } else {
      this.mostrarRango = false;
      this.propina =event.detail.value; 
    }
  }

  /* Funcion para guardar el valor de propina en el componente de selección libre*/
  cambioValorPropinaRango(event) {
     this.propina =event.detail.value;
  }

  /* Función para calcular el importe total por comensal */
  calcular()
  {
  	this.total = ((this.importe * this.propina)/100)+this.importe;
    this.totalComensal = this.total / this.numComensales;
    this.presentModal()
  } 

  /* Función para visualizar el modal con el restultado*/
  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: ModalPage,
      cssClass: 'my-custom-class',
      componentProps: {
        'total': this.total,
        'totalComensal': this.totalComensal,
        'numeroComensales': this.numComensales
      }
    });
    return await modal.present();
  }
  
}
